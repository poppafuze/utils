#!/bin/bash
#
# sets up Fedora after install
#
mkdir /root/tmp #need to be root for anyway for bootstrap items below
# passwordless sudo via wheel
# would be visudo with script as EDITOR, needs --enable-env-editor
# write to a tmp, check it, then move it over
# be root here due to bootstrap problem
# set editor back to vim
sudo dnf remove -y nano-default-editor
sudo dnf install -y vim-default-editor
