#!/usr/bin/bash
#
# checkefi
#
# license:  GPLv2
#

### shell run options
#set -o pipefail  #return any nonzero code in pipe as retval for whole pipe
set -u  #fail+exit on any unset var
#set -e  #exit on any fail
#set -x  #debug
#IFS=$' '

## vars
rethappy=0
retcode=4

trap exit_cleanup EXIT  #prepare for cleanup from anywhere

# all functions called from here
main () {
    checkefi
    #exit $retcode
    exit $rethappy
}

#all functions defined
printusage () {
  echo "
   Usage:
    - no CLI input/argument is required or accepted
    - do some common stuff for a newly-installed machine
"
}

checkefi () {
    [[ -f "/sys/firmware/efi" ]] && echo "efi on" || echo "no efi found"
}

exit_cleanup () {
    echo "exiting with $retcode"
    exit $retcode
}

### do everything
main
